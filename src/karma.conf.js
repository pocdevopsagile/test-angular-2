// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function(config) {
  const process = require('process');
  process.env.CHROME_BIN = require('puppeteer').executablePath();
  config.set({
    basePath: "",
    frameworks: ["jasmine", "@angular-devkit/build-angular"],
    plugins: [
      require("karma-jasmine"),
      require("karma-chrome-launcher"),
      require("karma-mocha-reporter"),
      require("karma-jasmine-html-reporter"),      
      require("karma-coverage-istanbul-reporter"),
      require('karma-sonarqube-reporter'),
      require("karma-junit-reporter"),
      require('puppeteer'),
      require("@angular-devkit/build-angular/plugins/karma")
    ],
    client: {
      clearContext: false
    },

    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, './coverage'),
      reports: ['html', 'lcovonly','cobertura'],
      fixWebpackSourcePaths: true
    },

    reporters: ['progress','mocha','junit'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    singleRun: false,   
    restartOnFileChange: true,
    
    captureTimeout: 5000 ,
    browserDisconnectTolerance: 5,    
    browserDisconnectTimeout: 2000 ,
    browserNoActivityTimeout: 2000 ,
    

    angularCli: {
      environment: 'dev',
      codeCoverage: true
    },
    
    browsers: ['ChromeHeadlessNoSandbox'],
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
      base: 'ChromeHeadless',
      flags: ['--no-sandbox']
      }
    },
    junitReporter:{
      outputDir: '',
      outputFile: undefined,
      suite: '',
      useBrowserName: true,
      nameFormatter: undefined,
      classNameFormatter: undefined,
      properties: {},
      }
  });
};
